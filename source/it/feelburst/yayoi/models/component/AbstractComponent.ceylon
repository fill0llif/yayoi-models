import it.feelburst.yayoi.models {
	Named
}
import it.feelburst.yayoi.models.visitor {
	Visitor
}

"Component representing states and behaviours shared between
 component, container and window"
shared sealed interface AbstractComponent
	satisfies
		Named&
		Hierarchical&
		Listenable {
	"Whether or not is valid or needs to be updated"
	shared formal Boolean valid;
	"X coordinate of this component"
	shared formal Integer x;
	"Y coordinate of this component"
	shared formal Integer y;
	"This component's width"
	shared formal Integer width;
	"This component's height"
	shared formal Integer height;
	"Whether the component is visible or not"
	shared formal Boolean visible;
	"Invalidate this component"
	shared formal void invalidate(Boolean internal = true);
	"Validate this component (components layout and UIs)"
	shared formal void validate(Boolean internal = true);
	"Allow a visitor to visit this component"
	shared formal void accept(Visitor visitor);
}