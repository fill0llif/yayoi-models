
shared interface ComponentMutator
	satisfies MutableListenable {
	"Set size of this component"
	shared formal void setSize(Integer width, Integer height);
	"Set location of this component"
	shared formal void setLocation(Integer x, Integer y);
	"Center this component within its container or the screen"
	shared formal void center();
	"Display this component"
	shared formal void display();
	"Hide this component"
	shared formal void hide();
}