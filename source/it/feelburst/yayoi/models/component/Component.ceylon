import it.feelburst.yayoi.models {
	Value
}
import it.feelburst.yayoi.models.visitor {
	Visitor
}
import it.feelburst.yayoi.models.component {
	AbstractComponent
}
"A component that can be rendered on screen within a container"
shared interface Component<out Type>
	satisfies
		AbstractComponent&
		Value<Type>
	given Type satisfies Object {
	shared actual void accept(Visitor visitor) =>
		visitor.visitComponent(this);
}