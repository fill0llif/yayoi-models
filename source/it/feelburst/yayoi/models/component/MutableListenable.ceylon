import ceylon.collection {
	MutableSet
}

import it.feelburst.yayoi.models.listener {
	Listener
}
shared interface MutableListenable
	satisfies Listenable {
	"Component's listeners"
	shared formal actual MutableSet<Listener<Object>> listeners;
}
