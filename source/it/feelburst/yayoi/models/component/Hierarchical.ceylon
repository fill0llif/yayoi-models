import it.feelburst.yayoi.models.collection {
	AbstractCollection
}

"A component of a tree"
shared sealed interface Hierarchical {
	"Component parent"
	shared formal variable AbstractCollection? parent;
	"Component root"
	shared default AbstractCollection? root =>
		if (exists prnt = parent) then
			prnt.root
		else
			null;
}
