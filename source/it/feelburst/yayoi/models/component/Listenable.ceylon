import it.feelburst.yayoi.models.listener {
	Listener
}
shared interface Listenable {
	"Component's listeners"
	shared formal Set<Listener<Object>> listeners;
}
