shared interface AbstractMutableComponent
	satisfies
		AbstractComponent&
		ComponentMutator {}