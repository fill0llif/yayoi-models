import it.feelburst.yayoi.models {
	Named,
	Value
}
import it.feelburst.yayoi.models.component {
	AbstractComponent
}
"A listener that listens to a component event"
shared interface Listener<out Type>
	satisfies
		Named&
		Value<Type>
	given Type satisfies Object {
	shared formal Set<AbstractComponent&Value<Object>> components;
}
