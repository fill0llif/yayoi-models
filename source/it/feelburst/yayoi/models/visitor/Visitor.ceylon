import it.feelburst.yayoi.models.collection {
	Collection
}
import it.feelburst.yayoi.models.component {
	Component
}
import it.feelburst.yayoi.models.container {
	Container
}
import it.feelburst.yayoi.models.window {
	Window
}
shared interface Visitor {
	shared formal void visitComponent<Type>(
		Component<Type> visited);
	shared formal void visitContainer<Type>(
		Container<Type> visited);
	shared formal void visitCollection<Type>(
		Collection<Type> visited);
	shared formal void visitWindow<Type>(
		Window<Type> visited);
	shared default void visitRoot<Type>(
		Window<Type>|Collection<Object> visited) {
		switch (visited)
		case (is Window<Object>) {
			visitWindow(visited);
		}
		else {
			visitCollection(visited);
		}
	}
}
