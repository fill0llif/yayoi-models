import it.feelburst.yayoi.models.collection {
	Collection
}
import it.feelburst.yayoi.models.component {
	Component
}
import it.feelburst.yayoi.models.container {
	Container
}
import it.feelburst.yayoi.models.visitor {
	Visitor
}
import it.feelburst.yayoi.models.window {
	Window
}
shared abstract class AbstractVisitor()
	satisfies Visitor {
	shared default actual void visitComponent<Type>(
		Component<Type> visited) {}
	shared default actual void visitContainer<Type>(
		Container<Type> visited) {}
	shared default actual void visitCollection<Type>(
		Collection<Type> visited) {}
	shared default actual void visitWindow<Type>(
		Window<Type> visited) {}
}