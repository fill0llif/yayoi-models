import it.feelburst.yayoi.models.collection {
	AbstractCollection
}
import it.feelburst.yayoi.models.component {
	AbstractComponent
}
shared final class DepthFirstTraversalVisitor(
	shared actual Visitor visitor)
	extends AbstractChildrenTraversalVisitor() {
	shared actual {AbstractComponent*} children(AbstractCollection clt) =>
		clt;
}
