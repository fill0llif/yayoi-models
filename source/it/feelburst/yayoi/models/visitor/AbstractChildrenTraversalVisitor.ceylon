import it.feelburst.yayoi.models.collection {
	Collection,
	AbstractCollection
}
import it.feelburst.yayoi.models.component {
	Component,
	AbstractComponent
}
import it.feelburst.yayoi.models.container {
	Container
}
import it.feelburst.yayoi.models.visitor {
	AbstractVisitor,
	Visitor
}
import it.feelburst.yayoi.models.window {
	Window
}
shared abstract class AbstractChildrenTraversalVisitor()
	extends AbstractVisitor() {
	
	shared formal Visitor visitor;
	
	shared default actual void visitComponent<Type>(Component<Type> visited) {
		visitor.visitComponent(visited);
	}
	shared default actual void visitContainer<Type>(
		Container<Type> visited) {
		visitor.visitContainer(visited);
		visitAbstractCollection(visited);
	}
	shared default actual void visitCollection<Type>(Collection<Type> visited) {
		visitor.visitCollection(visited);
		visitAbstractCollection(visited);
	}
	shared default actual void visitWindow<Type>(Window<Type> visited) {
		visitor.visitWindow(visited);
		visitAbstractCollection(visited);
	}
	
	shared default void visitAbstractCollection(AbstractCollection clt) {
		children(clt).each((AbstractComponent cmpnt) {
			if (is Container<Object> cmpnt) {
				visitContainer(cmpnt);
			}
			else if (is Collection<Object> cmpnt) {
				visitCollection(cmpnt);
			}
			else {
				assert (is Component<Object> cmpnt);
				visitComponent(cmpnt);
			}
		});
	}
	
	shared formal {AbstractComponent*} children(AbstractCollection clt);
	
}