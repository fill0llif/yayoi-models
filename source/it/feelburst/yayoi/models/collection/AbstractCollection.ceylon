import it.feelburst.yayoi.models {
	Value
}
import it.feelburst.yayoi.models.component {
	AbstractComponent
}
"Component representing states and behaviours of a collection
 that can contain multiple components"
see(`interface AbstractComponent`)
shared sealed interface AbstractCollection
	satisfies
		AbstractComponent&
		Set<AbstractComponent&Value<Object>> {}
