import it.feelburst.yayoi.models {
	Value
}
import it.feelburst.yayoi.models.visitor {
	Visitor
}
"A collection that can be rendered on screen within a container/collection
 and that can render its components without control on the layout"
shared interface Collection<out Type>
	satisfies
		AbstractCollection&
		Value<Type>
	given Type satisfies Object {
	shared actual void accept(Visitor visitor) =>
		visitor.visitCollection(this);
}
