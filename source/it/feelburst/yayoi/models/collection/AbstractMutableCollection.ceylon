import ceylon.collection {
	MutableSet
}

import it.feelburst.yayoi.models {
	Value
}
import it.feelburst.yayoi.models.component {
	AbstractComponent,
	AbstractMutableComponent
}
see(`interface AbstractComponent`)
shared sealed interface AbstractMutableCollection
	satisfies
		AbstractCollection&
		AbstractMutableComponent&
		MutableSet<AbstractComponent&Value<Object>> {}
