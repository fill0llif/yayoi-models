import it.feelburst.yayoi.models.collection {
	AbstractMutableCollection
}

shared interface MutableContainer<out Type>
	satisfies
		Container<Type>&
		AbstractMutableCollection
	given Type satisfies Object {
	shared formal void doLayout();
}
