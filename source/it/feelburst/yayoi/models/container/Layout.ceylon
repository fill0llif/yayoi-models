import it.feelburst.yayoi.models {
	Named,
	Value
}
"A layout manager that defines how the components of a container are
 rendered on screen"
shared interface Layout<out Type>
	satisfies
		Named&
		Value<Type>
	given Type satisfies Object {}
