import it.feelburst.yayoi.models {
	Value
}
import it.feelburst.yayoi.models.collection {
	AbstractCollection
}
import it.feelburst.yayoi.models.container {
	Layout
}
import it.feelburst.yayoi.models.visitor {
	Visitor
}

"A container that can be rendered on screen within a container
 and that can render its components using a layout manager"
shared interface Container<out Type>
	satisfies
		AbstractCollection&
		Value<Type>
	given Type satisfies Object {
	shared formal variable Layout<Object>? layout;
	shared default actual void accept(Visitor visitor) =>
		visitor.visitContainer(this);
}
