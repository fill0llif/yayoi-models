import it.feelburst.yayoi.models {
	Value
}
shared interface WindowState<out Type>
	satisfies Value<Type> {}
