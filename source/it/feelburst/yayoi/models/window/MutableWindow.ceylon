import it.feelburst.yayoi.models.collection {
	AbstractMutableCollection
}
import it.feelburst.yayoi.models.visitor {
	Visitor
}

"A window that can be rendered on screen and can contains
 many containers, collections or components"
shared interface MutableWindow<out Type>
	satisfies
		Window<Type>&
		AbstractMutableCollection
	given Type satisfies Object {
	"Set title of this window"
	shared formal void setTitle(String title);
	"Set exit app on close on this window"
	shared formal void setExitOnClose();
	"Set dispose window on close"
	shared formal void setDisposeOnClose();
	"Set hide window on close"
	shared formal void setHideOnClose();
	"Iconify this window"
	shared formal void iconify();
	"Maximize this window"
	shared formal void maximize();
	"Restore this window (neither iconified nor maximized)"
	shared formal void restore();
	"Close this window"
	shared formal void close();
	"Pack this window"
	shared formal void pack();
	
	shared default actual void accept(Visitor visitor) =>
		visitor.visitWindow(this);
}
