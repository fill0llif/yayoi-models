import it.feelburst.yayoi.models {
	Value
}
import it.feelburst.yayoi.models.collection {
	AbstractCollection
}
import it.feelburst.yayoi.models.visitor {
	Visitor
}
import it.feelburst.yayoi.models.window {
	WindowState
}

"A window that can be rendered on screen and can contains
 many containers, collections or components"
shared interface Window<out Type>
	satisfies
		AbstractCollection&
		Value<Type>
	given Type satisfies Object {
	"This window's title"
	shared formal String? title;
	"This window's state"
	shared formal WindowState<Object> state;
	"Whether this window is visible or it has been disposed of"
	shared formal Boolean opened;
	"Whether this window is maximized or not"
	shared formal Boolean maximized;
	"Whether this window is iconified or not"
	shared formal Boolean iconified;
	"Whether this window is neither maximized or iconified or not"
	shared formal Boolean normal;
	"Whether this window has been disposed of or not"
	shared formal Boolean closed;
	
	shared default actual void accept(Visitor visitor) =>
		visitor.visitWindow(this);
}
